from src.game import *
from src.start_screen import *

if __name__ == '__main__':
    start_screen = StartScreen();
    start_screen.run()

    game = Game()
    game.run()