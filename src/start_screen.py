import pygame as pg
import sys
from assets.settings import *
from src.engine.button import *
from src.game import *

def get_font(size): # Returns Press-Start-2P in the desired size
    return pg.font.Font(None, size)
class StartScreen:
    def __init__(self):
        self.play_button = None
        self.menu_rect = None
        self.menu_text = None
        self.mouse_pos = None
        pg.init()
        pg.mouse.set_visible(True)
        self.screen = pg.display.set_mode(RES)
        self.background_image = pg.transform.scale(pg.image.load("assets/resources/background.png"), RES)
    def draw(self):

        self.screen.blit(self.background_image, (0, 0))

        self.mouse_pos = pg.mouse.get_pos()

        xPOS = HALF_WIDTH - 50
        self.menu_text = get_font(100).render("Alpha World", True, "#b68f40")
        self.menu_rect = self.menu_text.get_rect(center=(xPOS, 100))

        self.play_button = Button(image=pg.image.load("assets/resources/buttons/rect.png"), pos=(xPOS, HALF_HEIGHT-50),
                                  text_input="PLAY", font=get_font(75), base_color="#d7fcd4", hovering_color="green")
        self.screen.blit(self.menu_text, self.menu_rect)

    def update(self):
       # for button in [self.PLAY_BUTTON]:
        self.play_button.changeColor(self.mouse_pos)
        self.play_button.update(self.screen)
        pg.display.update()

    def check_events(self):
        for event in pg.event.get():
            if event.type == pg.QUIT or (event.type == pg.KEYDOWN and event.key == pg.K_ESCAPE):
                pg.quit()
                sys.exit()
            if event.type == pg.MOUSEBUTTONDOWN:
                if self.play_button.checkForInput(self.mouse_pos):
                    g = Game()
                    g.run()


    def run(self):
        while True:
            self.draw()
            self.update()
            self.check_events()


