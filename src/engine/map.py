import pygame as pg

class Map:
    def __init__(self, game):
        self.game = game
        self.mini_map = None
        self.world_map = {}
        self.name = ''

    def load_map(self, name):
        self.name = name
        if self.name == 'level_0':
            import assets.maps.level_0
            self.mini_map = assets.maps.level_0.mini_map
        elif self.name == 'base_level_0':
            import assets.maps.base_level_0
            self.mini_map = assets.maps.base_level_0.mini_map
        self.rows = len(self.mini_map)
        self.cols = len(self.mini_map[0])
        self.get_map()

    def get_map(self):
        for j, row in enumerate(self.mini_map):
            for i, value in enumerate(row):
                if value:
                    self.world_map[(i, j)] = value

    def draw(self):
        for pos in self.world_map:
            if self.world_map[pos] == 1:
                pg.draw.rect(self.game.screen, 'darkgray', (pos[0] * 100, pos[1] * 200, 100, 100), 2)
            else:
                pg.draw.rect(self.game.screen, 'red', (pos[0] * 100, pos[1] * 100, 100, 100), 2)