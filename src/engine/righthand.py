from src.engine.sprite_object import *


class RightHand(AnimatedSprite):
    def __init__(self, game, path='assets/resources/sprites/weapon/right_hand/00.png', scale=0.4, animation_time=35):
        super().__init__(game=game, path=path, scale=scale, animation_time=animation_time)
        self.images = deque(
            [pg.transform.smoothscale(img, (int(self.image.get_width() * scale), int(self.image.get_height() * scale)))
             for img in self.images])
        self.weapon_pos = (HALF_WIDTH - self.images[0].get_width() // 2, HEIGHT - self.images[0].get_height())
        self.is_idle = True
        self.revert = False
        self.num_images = len(self.images)
        self.frame_counter = 0

    def animate_action(self):
        if not self.is_idle:
            if self.animation_trigger:
                if not self.revert:
                    self.images.rotate(-1)
                else:
                    self.images.rotate(1)
                self.image = self.images[0]
                self.frame_counter += 1
                if self.frame_counter == self.num_images and self.revert:
                    self.revert = False
                    self.is_idle = True
                    self.frame_counter = 0
                elif self.frame_counter == self.num_images and not self.revert:
                    self.revert = True
                    self.images.rotate(1)
                    self.frame_counter = 1

    def draw(self):
        self.game.screen.blit(self.images[0], self.weapon_pos)

    def update(self):
        self.check_animation_time()
        self.animate_action()
