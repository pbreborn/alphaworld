from src.engine.npc import *
from random import choices, randrange


class ObjectHandler:
    def __init__(self, game):
        self.game = game
        self.sprite_list = []
        self.npc_list = []
        self.npc_sprite_path = 'assets/resources/sprites/npc/'
        self.static_sprite_path = 'assets/resources/sprites/static_sprites/'
        self.anim_sprite_path = 'assets/resources/sprites/animated_sprites/'
        add_sprite = self.add_sprite
        add_npc = self.add_npc
        self.npc_positions = {}


        # sprite map
        add_sprite(SpriteObject(game, path=self.static_sprite_path + 'bookshelf.png', scale=0.75, pos=(5.5, 4.75)))

    def check_win(self):
        return False

    def update(self):
        self.npc_positions = {npc.map_pos for npc in self.npc_list if npc.alive}
        [sprite.update() for sprite in self.sprite_list]
        [npc.update() for npc in self.npc_list]
        self.check_win()

    def add_npc(self, npc):
        self.npc_list.append(npc)

    def add_sprite(self, sprite):
        self.sprite_list.append(sprite)