import pygame as pg
import sys
from assets.settings import *
from src.engine.map import *
from src.engine.player import *
from src.engine.raycasting import *
from src.engine.object_renderer import *
from src.engine.sprite_object import *
from src.engine.object_handler import *
from src.engine.weapon import *
from src.engine.righthand import *
from src.engine.sound import *
from src.engine.pathfinding import *


class Game:
    def __init__(self):
        pg.init()
        pg.mouse.set_visible(False)
        self.screen = pg.display.set_mode(RES)
        self.clock = pg.time.Clock()
        self.delta_time = 1
        self.global_trigger = False
        self.global_event = pg.USEREVENT + 0
        pg.time.set_timer(self.global_event, 40)
        self.new_game()

    def new_game(self):
        self.map = Map(self)
        self.map.load_map('base_level_0')
        self.player = Player(self)
        self.object_renderer = ObjectRenderer(self)
        self.raycasting = RayCasting(self)
        self.object_handler = ObjectHandler(self)
        #self.weapon = Weapon(self)
        self.right_hand = RightHand(self)
        self.sound = Sound(self)
        self.pathfinding = PathFinding(self)
        pg.mixer.music.play(-1)

    def update(self):
        self.player.update()
        self.raycasting.update()
        self.object_handler.update()
        #self.weapon.update()
        self.right_hand.update()
        pg.display.flip()
        self.delta_time = self.clock.tick(FPS)
        pg.display.set_caption(f'{self.clock.get_fps() :.1f}')

    def draw(self):
       # self.screen.fill('black')
        self.object_renderer.draw()
        #self.weapon.draw()
        self.right_hand.draw()
    # self.map.draw()
       # self.player.draw()

    def check_events(self):
        self.global_trigger = False
        for event in pg.event.get():
            if event.type == pg.QUIT or (event.type == pg.KEYDOWN and event.key == pg.K_ESCAPE):
                pg.quit()
                sys.exit()
            elif event.type == self.global_event:
                self.global_trigger = True
            self.player.single_fire_event(event)

    def run(self):
        while True:
            self.check_events()
            self.update()
            self.draw()